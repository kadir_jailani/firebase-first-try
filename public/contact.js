(function () {
  // Contact Form
  var form = document.getElementById('contact-form');
  var formError = document.getElementById('form-error-box');
  var innerError = document.getElementById('form-error');
  var closeError;
  var database = firebase.database();

  formError.style.display = "none";
  form.addEventListener('submit', sendMessage);
  

  function sendMessage (event) {
    
    event.preventDefault();

    var name = document.getElementById('contact-name').value;
    var email = document.getElementById('contact-email').value;
    var message = document.getElementById('contact-message').value;
    console.log(name, email, message);
    
    if(name === "" | email === "" | message === "" ){
      innerError.innerHTML = ('<h3>Please make sure all form completed!</h3>' + '<button id="closeForm"><i class="fa fa-times" aria-hidden="true"></i></button>');
      formError.style.display = "block";
      
      closeError = document.getElementById('closeForm');
      closeError.addEventListener('click', closeThis);
      
      return false;
    }else{
      formError.style.display = 'none';
    }

    database.ref('messages').push({
      name: name,
      email: email,
      message: message
    });
    
    form.reset();
    
    function closeThis( event ){
      formError.style.display = 'none';
    }
  }

})();